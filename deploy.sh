#bin/bash

# Globals
HEAD="\033[0;36m[DAC - DEPLOY]\033[0m"
CONTEXT="./setup"

# Instances to deploy & port padding
mapfile -t instances < ./instances
padding=1

for i in "${instances[@]}"
do
  printf "$HEAD Deploying instance on $i\n"
  HOST="ubuntu@$i"
  
  # Creates dac directory if necessary
  ssh $HOST mkdir dac > /dev/null 2>&1
  
  # Copy files to remote server
  printf "$HEAD Copying files to remote server...\n"
  scp -i ~/.ssh/id_rsa "$CONTEXT/setup.sh" "$CONTEXT/Dockerfile" "$HOST:/home/ubuntu/dac"
  
  # Execute deploy
  printf "$HEAD Deploy completed, now starting setup...\n"
  ssh -t $HOST "cd dac/ && sudo ./setup.sh $padding"

  # Increment port padding
  ((padding++))
done
