#bin/bash

# Start prometheus
sudo docker run --rm -d --name prometheus -p 9090:9090 \
    -v /home/ubuntu/dac/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus