# DAC - MICKAEL GOMEZ

## Instances

J'ai travaillé avec 4 instances openstack :
* gomez-1-1 ➜ 172.28.100.186 (LoadBalancer)
* gomez-1-2 ➜ 172.28.100.64
* gomez-1-3 ➜ 172.28.100.77
* gomez-1-4 ➜ 172.28.100.22

## Serveur web

Simple serveur python
```bash
python -m http.server <PORT>
```

Image Docker basée sur Alpine
```Docker
FROM python:3.7-alpine
COPY index.html /

# Ces 2 lignes sont générées au déploiement
EXPOSE <PORT>
CMD python -m http.server <PORT>
```

## Déploiement

### Deux conditions de bon fonctionnement

Le fichier `instances` contient mes instances cibles
```
172.28.100.64
172.28.100.77
172.28.100.22
```

Ma clé ssh liée aux instances est ici : `~/.ssh/id_rsa`

### Script de déploiement

Je n'ai qu'à lancer `./deploy.sh`, et c'est tout : il va lui-même se charger de configurer mes instances, d'envoyer le contenu du dossier `setup/` sur chacune d'entre elles, de créer les images et containers (ou de les remplacer s'ils existent déjà), et de prendre en compte le padding au niveau du port à exposer sur la machine hôte.

*NB: Le padding du port se fait au niveau de l'interface entre le container et l'host, mais chaque service est toujours mappé sur le port 80 quoi qu'il arrive.*

## Loadbalancer

Loadbalancer basique géré par Nginx
```nginx
upstream balancers {
  server 172.28.100.64;
  server 172.28.100.77;
  server 172.28.100.22;
}

server {
  listen 80 default_server;
  listen [::]:80 default_server;

  server_name _;

  location / {
    proxy_pass http://balancers;
  }
}
```

## Monitoring

Le `node_exporter` est directement installé sur le loadbalancer manuellement (pour accèder facilement à toutes les métriques de l'host).

Prometheus et Grafana en revanche sont respectivement lancés grâce à un script `run.sh` dans `/home/ubuntu/dac/prometheus` et `/home/ubuntu/dac/grafana`, qui ne fait que créer un container à partir des images officielles.

Une fois lancés, tous les services sont disponibles aux ports par défaut :
* 3000 ➜ Grafana
* 9100 ➜ node_exporter
* 9090 ➜ Prometheus