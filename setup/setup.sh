#bin/bash

# Globals
HEAD="\033[1;32m[DAC - SETUP]\033[0m"
NAME=webserver
PORT_PADDING=$((8000 + $1))

# Generate html file containing hostname
echo "<h1>$(hostname)</h1>" > ./index.html

# Stop running container
existingContainer=$(sudo docker ps -aqf "name=$NAME")
if [ -n "${existingContainer}" ]
then
  printf "$HEAD Webserver's container ($existingContainer) already running, stoping it...\n"
  sudo docker stop "$existingContainer"
fi

# Remove existing image
existingImage=$(sudo docker images -q $NAME)
if [ -n "${existingImage}" ]
then
  printf "$HEAD Webserver's image ($existingImage) already exists, removing it...\n"
  sudo docker rmi "$existingImage"
fi

# Setup port padding
echo >> ./Dockerfile && echo "EXPOSE $PORT_PADDING" >> ./Dockerfile
echo "CMD python -m http.server $PORT_PADDING" >> ./Dockerfile

# Build image
printf "$HEAD Building image...\n"
sudo docker build -t $NAME .

# Run container
printf "$HEAD Starting container...\n"
sudo docker run --rm -d --name $NAME -p "80:$PORT_PADDING" $NAME

# All done
printf "$HEAD All done, closing connection, bye !\n"